0\r�m��   _   J%C^    https://openvsxorg.blob.core.windows.net/resources/tamasfe/even-better-toml/0.14.2/package.json{
  "name": "even-better-toml",
  "displayName": "Even Better TOML",
  "description": "Fully-featured TOML support",
  "version": "0.14.2",
  "repository": {
    "url": "https://github.com/tamasfe/taplo"
  },
  "bugs": {
    "url": "https://github.com/tamasfe/taplo/issues"
  },
  "publisher": "tamasfe",
  "author": {
    "name": "tamasfe"
  },
  "preview": true,
  "icon": "taplo-icon.png",
  "galleryBanner": {
    "color": "#731c12",
    "theme": "dark"
  },
  "license": "SEE LICENSE IN LICENSE.md",
  "engines": {
    "vscode": "^1.44.0"
  },
  "categories": [
    "Programming Languages",
    "Linters",
    "Formatters",
    "Other"
  ],
  "activationEvents": [
    "onLanguage:toml",
    "onLanguage:cargoLock",
    "onCommand:evenBetterToml.pasteTomlAsJson",
    "onCommand:evenBetterToml.copyTomlAsJson",
    "onCommand:evenBetterToml.pasteJsonAsToml",
    "onCommand:evenBetterToml.copyJsonAsToml",
    "onCommand:evenBetterToml.clearCache",
    "onCommand:evenBetterToml.downloadSchemas"
  ],
  "keywords": [
    "toml",
    "syntax",
    "rust",
    "formatter",
    "config"
  ],
  "extensionKind": [
    "workspace"
  ],
  "contributes": {
    "grammars": [
      {
        "language": "toml",
        "scopeName": "source.toml",
        "path": "./toml.tmLanguage.json"
      },
      {
        "scopeName": "markdown.toml.frontmatter.codeblock",
        "path": "./toml.frontmatter.tmLanguage.json",
        "injectTo": [
          "text.html.markdown"
        ]
      },
      {
        "scopeName": "markdown.toml.codeblock",
        "path": "./toml.markdown.tmLanguage.json",
        "injectTo": [
          "text.html.markdown"
        ],
        "embeddedLanguages": {
          "meta.embedded.block.toml": "toml"
        }
      }
    ],
    "languages": [
      {
        "id": "toml",
        "aliases": [
          "TOML"
        ],
        "extensions": [
          ".toml"
        ],
        "filenames": [
          "Cargo.lock"
        ],
        "configuration": "./language-configuration.json"
      }
    ],
    "menus": {
      "commandPalette": [
        {
          "command": "evenBetterToml.copyTomlAsJson",
          "when": "config.evenBetterToml.commands.copyTomlAsJson"
        },
        {
          "command": "evenBetterToml.pasteTomlAsJson",
          "when": "config.evenBetterToml.commands.pasteTomlAsJson"
        },
        {
          "command": "evenBetterToml.pasteJsonAsToml",
          "when": "config.evenBetterToml.commands.pasteJsonAsToml"
        },
        {
          "command": "evenBetterToml.clearCache",
          "when": "config.evenBetterToml.commands.clearCache"
        },
        {
          "command": "evenBetterToml.downloadSchemas",
          "when": "config.evenBetterToml.commands.downloadSchemas"
        },
        {
          "command": "evenBetterToml.debug.showSyntaxTree",
          "when": "editorLangId == toml && config.evenBetterToml.debug"
        }
      ]
    },
    "commands": [
      {
        "command": "evenBetterToml.copyTomlAsJson",
        "title": "TOML: Copy Selection as JSON",
        "enablement": "editorHasSelection"
      },
      {
        "command": "evenBetterToml.copyJsonAsToml",
        "title": "TOML: Copy JSON Selection as TOML",
        "enablement": "editorHasSelection"
      },
      {
        "command": "evenBetterToml.pasteTomlAsJson",
        "title": "TOML: Paste as JSON"
      },
      {
        "command": "evenBetterToml.pasteJsonAsToml",
        "title": "TOML: Paste JSON as TOML"
      },
      {
        "command": "evenBetterToml.clearCache",
        "title": "TOML: Clear Cache"
      },
      {
        "command": "evenBetterToml.downloadSchemas",
        "title": "TOML: Download All Schemas"
      },
      {
        "command": "evenBetterToml.debug.showSyntaxTree",
        "title": "TOML (debug): Show Syntax Tree",
        "enablement": "editorLangId == toml && config.evenBetterToml.debug"
      }
    ],
    "semanticTokenTypes": [
      {
        "id": "tomlArrayKey",
        "superType": "variable",
        "description": "Keys of regular arrays."
      },
      {
        "id": "tomlTableKey",
        "superType": "variable",
        "description": "Keys of inline tables."
      }
    ],
    "semanticTokenScopes": [
      {
        "scopes": {
          "tomlArrayKey": [
            "variable.key.array.toml"
          ],
          "tomlTableKey": [
            "variable.key.table.toml"
          ]
        }
      }
    ],
    "configuration": {
      "title": "Even Better TOML",
      "properties": {
        "evenBetterToml.activationStatus": {
          "description": "Show a status bar message while the extension is activating.",
          "type": "boolean",
          "scope": "resource",
          "default": true
        },
        "evenBetterToml.taploConfig": {
          "description": "An absolute, or workspace relative path to the Taplo configuration file.",
          "type": "string",
          "scope": "resource"
        },
        "evenBetterToml.taploConfigEnabled": {
          "description": "Whether to enable the usage of a Taplo configuration file.",
          "type": "boolean",
          "scope": "resource",
          "default": true
        },
        "evenBetterToml.semanticTokens": {
          "description": "Enable semantic tokens for inline table and array keys. If enabled, keys of arrays and inline tables will have the same color as table headers and array of tables headers.",
          "type": "boolean",
          "scope": "resource",
          "default": false
        },
        "evenBetterToml.commands.copyTomlAsJson": {
          "description": "Show the \"Copy Selection as JSON\" command in the palette.",
          "type": "boolean",
          "scope": "resource",
          "default": true
        },
        "evenBetterToml.commands.copyJsonAsToml": {
          "description": "Show the \"Copy JSON Selection as TOML\" command in the palette.",
          "type": "boolean",
          "scope": "resource",
          "default": true
        },
        "evenBetterToml.commands.pasteJsonAsToml": {
          "description": "Show the \"Paste JSON as TOML\" command in the palette.",
          "type": "boolean",
          "scope": "resource",
          "default": true
        },
        "evenBetterToml.commands.pasteTomlAsJson": {
          "description": "Show the \"Paste as JSON\" command in the palette.",
          "type": "boolean",
          "scope": "resource",
          "default": true
        },
        "evenBetterToml.commands.clearCache": {
          "description": "Show the \"Clear Cache\" command in the palette.",
          "type": "boolean",
          "scope": "resource",
          "default": false
        },
        "evenBetterToml.commands.downloadSchemas": {
          "description": "Show the \"Download All Schemas\" command in the palette.",
          "type": "boolean",
          "scope": "resource",
          "default": false
        },
        "evenBetterToml.formatter.alignEntries": {
          "description": "Align consecutive entries vertically.",
          "type": "boolean",
          "scope": "resource",
          "default": false
        },
        "evenBetterToml.formatter.alignComments": {
          "description": "Align comments vertically after entries and array values.",
          "type": "boolean",
          "scope": "resource",
          "default": true
        },
        "evenBetterToml.formatter.arrayTrailingComma": {
          "description": "Append trailing commas for multi-line arrays.",
          "type": "boolean",
          "scope": "resource",
          "default": true
        },
        "evenBetterToml.formatter.arrayAutoExpand": {
          "description": "Expand arrays to multiple lines that exceed the maximum column width.",
          "type": "boolean",
          "scope": "resource",
          "default": true
        },
        "evenBetterToml.formatter.arrayAutoCollapse": {
          "description": "Collapse arrays that don't exceed the maximum column width and don't contain comments.",
          "type": "boolean",
          "scope": "resource",
          "default": true
        },
        "evenBetterToml.formatter.compactArrays": {
          "description": "Omit white space padding from single-line arrays.",
          "type": "boolean",
          "scope": "resource",
          "default": true
        },
        "evenBetterToml.formatter.compactInlineTables": {
          "description": "Omit white space padding from the start and end of inline tables.",
          "type": "boolean",
          "scope": "resource",
          "default": false
        },
        "evenBetterToml.formatter.compactEntries": {
          "description": "Omit white space padding around `=` for entries.",
          "type": "boolean",
          "scope": "resource",
          "default": false
        },
        "evenBetterToml.formatter.columnWidth": {
          "description": "Maximum column width in characters, affects array expansion and collapse, this doesn't take whitespace into account.",
          "type": "number",
          "minimum": 0,
          "default": 80
        },
        "evenBetterToml.formatter.indentTables": {
          "description": "Indent based on tables and arrays of tables and their subtables, subtables out of order are not indented.",
          "type": "boolean",
          "scope": "resource",
          "default": false
        },
        "evenBetterToml.formatter.indentEntries": {
          "description": "Indent entries under tables.",
          "type": "boolean",
          "scope": "resource",
          "default": false
        },
        "evenBetterToml.formatter.indentString": {
          "description": "The substring that is used for indentation, should be tabs or spaces, but technically can be anything. Uses the IDE setting if not set.",
          "type": [
            "string",
            "null"
          ],
          "scope": "resource",
          "default": null
        },
        "evenBetterToml.formatter.reorderKeys": {
          "description": "Alphabetically reorder keys that are not separated by empty lines.",
          "type": "boolean",
          "scope": "resource",
          "default": false
        },
        "evenBetterToml.formatter.allowedBlankLines": {
          "description": "Maximum amount of allowed consecutive blank lines. This does not affect the whitespace at the end of the document, as it is always stripped.",
          "type": "integer",
          "scope": "resource",
          "minimum": 0,
          "default": 2
        },
        "evenBetterToml.formatter.trailingNewline": {
          "description": "Add trailing newline at the end of the file if not present.",
          "type": "boolean",
          "scope": "resource",
          "default": true
        },
        "evenBetterToml.formatter.crlf": {
          "description": "Use CRLF for line endings.",
          "type": "boolean",
          "scope": "resource",
          "default": false
        },
        "evenBetterToml.schema.enabled": {
          "description": "Enable completion and validation based on JSON schemas.",
          "type": "boolean",
          "scope": "resource",
          "default": true
        },
        "evenBetterToml.schema.links": {
          "description": "Enable editor links.",
          "type": "boolean",
          "scope": "resource",
          "default": false
        },
        "evenBetterToml.schema.repositoryEnabled": {
          "description": "Whether to use schemas from the provided schema repository.",
          "type": "boolean",
          "scope": "resource",
          "default": true
        },
        "evenBetterToml.schema.repositoryUrl": {
          "description": "A HTTP(S) URL that points to a schema index.",
          "type": "string",
          "scope": "resource",
          "default": "https://taplo.tamasfe.dev/schema_index.json"
        },
        "evenBetterToml.schema.associations": {
          "description": "Document and schema associations.",
          "markdownDescription": "Document and schema associations. \n\n The key must be a regular expression, this pattern is used to associate schemas with absolute document URIs. On multiple matches the first one is used in an undefined order. \n\n The value must be an absolute URI to the JSON schema, for supported values and more information [read here](https://taplo.tamasfe.dev/configuration#visual-studio-code).",
          "type": "object",
          "scope": "resource",
          "default": {
            "^(.*(/|\\\\)\\.?taplo\\.toml|\\.?taplo\\.toml)$": "taplo://taplo.toml"
          }
        },
        "evenBetterToml.actions.ignoreDeprecatedAssociations": {
          "description": "Whether to ignore deprecated builtin associations.",
          "type": "string",
          "scope": "resource",
          "default": false
        },
        "evenBetterToml.debug": {
          "description": "Enable features to debug the extension.",
          "type": "boolean",
          "scope": "resource",
          "default": false
        }
      }
    }
  },
  "main": "./dist/extension.js",
  "scripts": {
    "build:syntax": "ts-node src/syntax/index.ts",
    "vscode:prepublish": "yarn --ignore-engines build",
    "build": "yarn --ignore-engines rollup --silent -c rollup.config.js"
  },
  "dependencies": {
    "@taplo/lsp": "^0.2.4",
    "deep-equal": "^2.0.4",
    "encoding": "^0.1.13",
    "node-fetch": "^2.6.1",
    "vscode-languageclient": "^7.0.0"
  },
  "devDependencies": {
    "@rollup/plugin-commonjs": "^16.0.0",
    "@rollup/plugin-node-resolve": "^10.0.0",
    "@types/deep-equal": "^1.0.1",
    "@types/node": "^12.12.0",
    "@types/node-fetch": "^2.5.7",
    "@types/vscode": "^1.44.0",
    "rollup": "^2.33.1",
    "rollup-plugin-typescript2": "^0.29.0",
    "ts-node": "^8.10.2",
    "typescript": "^4.0.5"
  }
}
�A�Eo��   ��C6        EZ�-�,/ �/�,/ �  HTTP/1.1 200 OK Cache-Control: max-age=2592000 Content-Length: 13891 Content-Type: application/json Last-Modified: Tue, 13 Jul 2021 01:02:15 GMT ETag: 0x8D94599DB144FCC Server: Windows-Azure-Blob/1.0 Microsoft-HTTPAPI/2.0 x-ms-request-id: a4340123-601e-0046-32a2-c07076000000 x-ms-version: 2009-09-19 x-ms-lease-status: unlocked x-ms-blob-type: BlockBlob Access-Control-Allow-Origin: * Date: Thu, 14 Oct 2021 02:20:41 GMT      8  0�40�� ��0�2I�   ��0	*�H�� 0O10	UUS10U
Microsoft Corporation1 0UMicrosoft RSA TLS CA 020210906090832Z220906090832Z0"1 0U*.blob.core.windows.net0�"0	*�H�� � 0�
� ��dc"�B�T"��>q(�lT��q��UDWկ<��٭p	]րD�?�1l�s|�����#I��<A`��e�ղ�|�vm����Xz�(���e]�t�8��Q��wSnr�c��c}n�Gr�d�U�|~x?c�9)zg��?DN&첀4�2�\£>A�C}��m�|e� ��aC����|u>�.e+����cK��R���d&�F�\Al.Hg��K\F�g�B�)Sx���ٛ�'hu�P�n�  ��
40�
00�~
+�y�n�jh w F�U�u�� 0���i��},At��I�����p�mG  {�f��   H0F! ����?PGQ��S��J��<�et
߫�@�! �qP�R=��+Cy�
����َ�gw|b� u U����6J��W<S���8xp%/���  {�f�c   F0D x��5���鮡)	�b���3+
ls��� ;B��s�n�G�3'���=$0���4��:u��� v A�ʱ�"FJơ:	B�^N1���K�h�b��  {�f��   G0E! �Du�[�g+�R}R]�f�
�諊�6.'�a� ��2�8Vˮ��n�V�_��ɫ�T����*�R0'	+�7
00
+0
+0>	+�710/'+�7�چu����Ʌ���a���`�]���A�Pd'0��+{0y0S+0�Ghttp://www.microsoft.com/pki/mscorp/Microsoft%20RSA%20TLS%20CA%2002.crt0"+0�http://ocsp.msocsp.com0U@ڄ	tuy���5�V$���z0U��0�<U�30�/�*.blob.core.windows.net�'*.dub09prdstr08a.store.core.windows.net�*.blob.storage.azure.net�*.z1.blob.storage.azure.net�*.z2.blob.storage.azure.net�*.z3.blob.storage.azure.net�*.z4.blob.storage.azure.net�*.z5.blob.storage.azure.net�*.z6.blob.storage.azure.net�*.z7.blob.storage.azure.net�*.z8.blob.storage.azure.net�*.z9.blob.storage.azure.net�*.z10.blob.storage.azure.net�*.z11.blob.storage.azure.net�*.z12.blob.storage.azure.net�*.z13.blob.storage.azure.net�*.z14.blob.storage.azure.net�*.z15.blob.storage.azure.net�*.z16.blob.storage.azure.net�*.z17.blob.storage.azure.net�*.z18.blob.storage.azure.net�*.z19.blob.storage.azure.net�*.z20.blob.storage.azure.net�*.z21.blob.storage.azure.net�*.z22.blob.storage.azure.net�*.z23.blob.storage.azure.net�*.z24.blob.storage.azure.net�*.z25.blob.storage.azure.net�*.z26.blob.storage.azure.net�*.z27.blob.storage.azure.net�*.z28.blob.storage.azure.net�*.z29.blob.storage.azure.net�*.z30.blob.storage.azure.net�*.z31.blob.storage.azure.net�*.z32.blob.storage.azure.net�*.z33.blob.storage.azure.net�*.z34.blob.storage.azure.net�*.z35.blob.storage.azure.net�*.z36.blob.storage.azure.net�*.z37.blob.storage.azure.net�*.z38.blob.storage.azure.net�*.z39.blob.storage.azure.net�*.z40.blob.storage.azure.net�*.z41.blob.storage.azure.net�*.z42.blob.storage.azure.net�*.z43.blob.storage.azure.net�*.z44.blob.storage.azure.net�*.z45.blob.storage.azure.net�*.z46.blob.storage.azure.net�*.z47.blob.storage.azure.net�*.z48.blob.storage.azure.net�*.z49.blob.storage.azure.net�*.z50.blob.storage.azure.net0��U��0��0���������Mhttp://mscrl.microsoft.com/pki/mscorp/crl/Microsoft%20RSA%20TLS%20CA%2002.crl�Khttp://crl.microsoft.com/pki/mscorp/crl/Microsoft%20RSA%20TLS%20CA%2002.crl0WU P0N0B	+�7*0503+'http://www.microsoft.com/pki/mscorp/cps0g�0U#0��/��8�-�%�����l��0U%0++0	*�H�� � p|��a,�Cv*�/2.�o�X��n��ޤ��WyCTg@��0|�Z^)�^���F��a��*R	̕=X*�jIUS�Mh���'��A1d��c���y
e��=��A��(��E�@�80�|�����h�p��R\A�v��;U�� ����.�Z�BX^�?μFH��$"�/ڗ"P^q�
c�1�G���NAb�py>�t���%U��w�2֔1]Cq���Nͪ��R��d����R�B�QS�ث���3�d2YV��NKE�ŇRg��z�� Ww����tby�os4bI��%艙
=.��Nz�:��Z�]Bs�� +h��K�C��M�|G�<�N呧�`�]"�\Z
�� �	 ����yܹ�O�%jO��8�8Y�������Ψ��=�IM���xy��h[�)Fo�o)�����?���>���Ф�,�ֱ�v�iA6�}��Wg����D��z�՛�!��x�uZ�=�������^  0�Z0�B��G"�=��X���J:0	*�H�� 0Z10	UIE10U
	Baltimore10U
CyberTrust1"0 UBaltimore CyberTrust Root0200721230000Z241008070000Z0O10	UUS10U
Microsoft Corporation1 0UMicrosoft RSA TLS CA 020�"0	*�H�� � 0�
� ��Y�*$|!1����j����J/�]�.)8�̕])��]7� ״?X:z��<�+
r�@�닎�������<%�l3�VO���H�����8o�� ��2*l)�]n�#��X�����mG��@�}sB��ZFK�)O>}�9|^#�gT�((�����qi�8IV�r#���)87fz ��1���A'�S��>��eڔ�]s4�hj����N]u��D[�}�*j�蟿�w
�k���n�'�@k)U&G�#�_��,Q�:�{�Z���K�B�e+�"6V(���(�$�^X�B>�'$!*\�*0�|���e
@*��c��/�Yz���C�ԁ�][F4/Ԣ��"G�y���6��1o�m ��K�h��Y�|���J�pLy��"�%Sد��O[�Y���)ZŁ�4���㡟�ཱུ�ەI�b�2e��<�~b�j��{ۇ�v����/0nf�QdN��í�7#�ͩj~-�n|�I����uB�Sꌟ ��%0�!0U�/��8�-�%�����l��0U#0��Y0�GX̬�T6�{:�M�0U��0U%0++0U�0� 04+(0&0$+0�http://ocsp.digicert.com0:U3010/�-�+�)http://crl3.digicert.com/Omniroot2025.crl0*U #0!0g�0g�0	+�7*0	*�H�� � ���z��5�t� �7��K�^.1�5�;q��YӉ�KA�Cr����R3Ry���ڔ����v���� �����K#��)A��5�vCn����>>�d2:|�v�=���/%ByC>������+CŨ���*�z��u]�q��"5%�n��R�a�D|�c-�����Ck`��
��q��Q+Q��{޸�|�Jp4����Fj?���>X�RdmAn��O��U�5ȡ�t[�Dе�O��*wげ��|	  {  0�w0�_�  �0	*�H�� 0Z10	UIE10U
	Baltimore10U
CyberTrust1"0 UBaltimore CyberTrust Root0000512184600Z250512235900Z0Z10	UIE10U
	Baltimore10U
CyberTrust1"0 UBaltimore CyberTrust Root0�"0	*�H�� � 0�
� ��"��=W�&r��y�)��蕀���[�+)�dߡ]��	m�(.�b�b�����8�!��A+R{�w��Ǻ���j	�s�@����b�-��PҨP�(���%�����g�?���R/��pp����˚���3zw����hDBH��¤�^`������Y�Y�c��c��}]�z�����^�>_��i��96ru�wRM�ɐ,�=��#S?$�!\�)��:��n�:k�tc3�h1�x�v����]*��M��'9 �E0C0U�Y0�GX̬�T6�{:�M�0U�0�0U�0	*�H�� � �]��oQhB�ݻO'%���d�-�0���))�y?v�#�
�X��ap�aj��
�ż0|��%��@O�̣~8�7O��h1�Lҳt�u^Hp�\��y����e����R79թ1z��*����E��<^����Ȟ|.Ȥ�NKm�pmkc�d�����.���P�s������2�����~5���>0�z�3=�e����b�GD,]��2�G�8.����2j��<����$B�c9�     0�P 
   20.60.40.4  �          �.�8�Q�~P��M	ܬl��ޏ�F�3�5�5J�A�Eo��   �I7F      
0\r�m��   Z   ��n\    https://openvsxorg.blob.core.windows.net/resources/muuvmuuv/vscode-sundial/2.6.2/README.md<h1 align="left">
  <img align="right" src="https://raw.githubusercontent.com/muuvmuuv/vscode-sundial/main/assets/icon.png" width="150">
  <b>🌚 Sundial 🌝</b>
</h1>

#### Change your VS Code theme/settings based on your sunset, sunrise, system appearance or other preferences!

[![Visual Studio Marketplace](https://vsmarketplacebadge.apphb.com/version-short/muuvmuuv.vscode-sundial.svg)](https://marketplace.visualstudio.com/items?itemName=muuvmuuv.vscode-sundial)
[![Visual Studio Marketplace](https://vsmarketplacebadge.apphb.com/installs-short/muuvmuuv.vscode-sundial.svg)](https://marketplace.visualstudio.com/items?itemName=muuvmuuv.vscode-sundial)
[![Visual Studio Marketplace](https://vsmarketplacebadge.apphb.com/rating-star/muuvmuuv.vscode-sundial.svg)](https://marketplace.visualstudio.com/items?itemName=muuvmuuv.vscode-sundial)
[![Maintainability](https://api.codeclimate.com/v1/badges/52f93dc5f852410ef448/maintainability)](https://codeclimate.com/github/muuvmuuv/vscode-sundial/maintainability)
[![Total alerts](https://img.shields.io/lgtm/alerts/g/muuvmuuv/vscode-sundial.svg?logo=lgtm&logoWidth=18)](https://lgtm.com/projects/g/muuvmuuv/vscode-sundial/alerts/)
[![Language grade: JavaScript](https://img.shields.io/lgtm/grade/javascript/g/muuvmuuv/vscode-sundial.svg?logo=lgtm&logoWidth=18)](https://lgtm.com/projects/g/muuvmuuv/vscode-sundial/context:javascript)

- [Installation](#installation)
- [Keybindings](#keybindings)
- [Commands](#commands)
- [Settings](#settings)
    - [Automatically set by current location](#automatically-set-by-current-location)
    - [Automatically set by latitude and longitude](#automatically-set-by-latitude-and-longitude)
    - [Automatically set by OS appearance](#automatically-set-by-os-appearance)
  - [Execution order](#execution-order)
  - [Examples](#examples)
- [Development](#development)
  - [Deploy](#deploy)
  - [Commits](#commits)
  - [Releases](#releases)

Sundial changes your theme and VS Code settings (if needed) based on your day and night cycle or
other options, you choose. It is inspired by the
[OSX Mojave dynamic backgrounds](https://www.apple.com/de/macos/mojave/) and
[Night Owl for Mac](https://nightowl.kramser.xyz/). It should _reduce eye pain_ when working in the
night or on the day. Humans should not strain their eyes too much, it's **not recommended** to have
a light theme in the night and vice versa.

Whenever you have ideas for this project, things you would like to add or you found a bug, feel free
to create an issue or start contributing! 😇

<p>
  <a href="https://www.buymeacoffee.com/devmuuv" target="_blank">
    <img src="https://www.buymeacoffee.com/assets/img/custom_images/yellow_img.png" alt="Buy me a Gluten-free Bread" />
  </a>
</p>

![VSCode Sundial](https://raw.githubusercontent.com/muuvmuuv/vscode-sundial/main/assets/banner.jpg)

## Installation

You can simply install any VS Code extension via the VS Code Marketplace. Just click the banner
below:

<p>
  <a href="https://marketplace.visualstudio.com/items?itemName=muuvmuuv.vscode-sundial">
    <img src="https://img.shields.io/badge/install-vscode_extension-blue.svg?style=for-the-badge" alt="Install VS Code extension Sundial">
  </a>
</p>

> ⚠️ IMPORTANT: Since VS Code 1.42.0 automatically changing the theme based on OS appearance is
> build in, if you want to use this plugin anyway you must disable this options with
> `"window.autoDetectColorScheme": false`

## Keybindings

**Sundial** contributes the following keybindings:

| Platform | Keybinding            | Description                           |
| -------- | --------------------- | ------------------------------------- |
| Windows  | <kbd>ctrl+alt+t</kbd> | Toggles between your day/night theme. |
| Mac      | <kbd>ctrl+cmd+t</kbd> | Toggles between your day/night theme. |

> Note: Whenever you use one of these keybindings, Sundial will be disabled.

## Commands

**Sundial** contributes the following commands:

| Command                           | Description                                 |
| --------------------------------- | ------------------------------------------- |
| Sundial: Switch to night theme 🌑 | Switches to your night theme.               |
| Sundial: Switch to day theme 🌕   | Switches to your day theme.                 |
| Sundial: Toggle Day/Night Theme   | Toggles between your day/night theme.       |
| Sundial: Enable extension         | Continues automation and enables extension. |
| Sundial: Disable extension        | Disables extension.                         |
| Sundial: Pause until next circle  | Pause until next day/night circle.          |

> Note: Whenever you use one of the first three commands, Sundial will be disabled.

## Settings

**Sundial** contributes the following settings:

| Setting                              | Default          | Description                                                               |
| ------------------------------------ | ---------------- | ------------------------------------------------------------------------- |
| `workbench.preferredLightColorTheme` | _Default Light+_ | Name of the theme of choice for your day work.                            |
| `workbench.preferredDarkColorTheme`  | _Default Dark+_  | Name of the theme of choice for your night work.                          |
| `sundial.sunrise`                    | _07:00_          | Set a time when your day starts in **24 hours format**.                   |
| `sundial.sunset`                     | _19:00_          | Set a time when your night starts in **24 hours format**.                 |
| `sundial.dayVariable`                | _0_              | Set a variable to change the theme **X minutes** before or after sunrise. |
| `sundial.nightVariable`              | _0_              | Set a variable to change the theme **X minutes** before or after sunset.  |
| `sundial.daySettings`                | _{}_             | An **object** of VSCode settings applied on the day.                      |
| `sundial.nightSettings`              | _{}_             | An **object** of VSCode settings applied on the night.                    |
| `sundial.interval`                   | _5_              | Set a interval in which sundial should check the time in **minutes**.     |

> ⚠️ Don't forget to set `"window.autoDetectColorScheme": false`

> If you set the interval to zero (0) sundial will not periodically check the time but still when VS
> Code triggers some editor events.

> On both `daySettings` and `nightSettings` they will override your Workbench VSCode settings.
> Please make sure both have the same properties otherwise they will not change since Sundial is not
> remembering the settings you have set before!

#### Automatically set by current location

Sundial will get your geolocation from [Free IP Geolocation API](https://freegeoip.app/) and check
your internet connection via [Cloudflares 1.1.1.1 DNS-Server](https://1.1.1.1/).

| Setting              | Default | Description                                      |
| -------------------- | ------- | ------------------------------------------------ |
| `sundial.autoLocale` | _false_ | Updates your location based on your geolocation. |

#### Automatically set by latitude and longitude

You can get your geolocation here: [Free IP Geolocation API](https://freegeoip.app/)

| Setting             | Default | Description        |
| ------------------- | ------- | ------------------ |
| `sundial.latitude`  | _⊘_     | e.g. _"50.110924"_ |
| `sundial.longitude` | _⊘_     | e.g. _"8.682127"_  |

#### Automatically set by OS appearance

Since VS Code version 1.42.0 it is now build in so you don't need this extension for this options.

```json
{
  "window.autoDetectColorScheme": true
}
```

Read more about the implementation here:

- https://github.com/microsoft/vscode/issues/61519
- https://github.com/microsoft/vscode/pull/86600
- https://github.com/microsoft/vscode/pull/87405

### Execution order

Sundial will check your settings in the following order and if one setting is present the next
coming will be ignored.

1. `sundial.latitude` and `sundial.longitude`
2. `sundial.autoLocale`
3. `sundial.sunrise` and `sundial.sunset`

### Examples

```jsonc
{
  "window.autoDetectColorScheme": false, // required!
  "workbench.preferredLightColorTheme": "Default Light+",
  "workbench.preferredDarkColorTheme": "Default Dark+",
  "sundial.interval": 20,
  "sundial.autoLocale": true
}
```

```jsonc
{
  "window.autoDetectColorScheme": false, // required!
  "workbench.preferredLightColorTheme": "Default Light+",
  "workbench.preferredDarkColorTheme": "Default Dark+",
  "sundial.sunrise": "05:12"
}
```

```jsonc
{
  "window.autoDetectColorScheme": false, // required!
  "workbench.preferredLightColorTheme": "Default Light+",
  "workbench.preferredDarkColorTheme": "Default Dark+",
  "sundial.dayVariable": 43,
  "sundial.latitude": "50.110924",
  "sundial.longitude": "8.682127",
  "sundial.daySettings": {
    "editor.fontSize": 13
  },
  "sundial.nightSettings": {
    "editor.fontSize": 15
  }
}
```

## Development

We are working with [webpack](https://webpack.js.org/) to bundle Sundial to the smallest possible
size to increase the load time in VSCode.

1.  Install packages via (your preferred package manager) `npm run install`
2.  Set `sundial.debug` to `2`
3.  Run debugger => `Launch Extension`
    - View the _Extension Host_ and adjust settings to test **or**
    - Change a file and save it, let _webpack_ compile
4.  Commit your changes with a detailed explanation
5.  Create a pull request

### Deploy

Sundial is deployed on VS Code Marketplace and Open VSX.

- VS Code Marketplace: `vsce publish`
- Open VSX: `pnpm publish-ovsx`

### Commits

Sundial follows the `config-conventional` spec.

### Releases

Run the below to create a new release. This will increase the version based on your changes and
create a new CHANGELOG.md section.

```shell
pnpm release
```
�A�Eo��   ����&        E`�u�,/ �V�u�,/ �  HTTP/1.1 200 OK Cache-Control: max-age=2592000, public Content-Length: 9974 Content-Type: text/plain Last-Modified: Tue, 28 Sep 2021 17:06:16 GMT ETag: 0x8D982A2488EC708 Server: Windows-Azure-Blob/1.0 Microsoft-HTTPAPI/2.0 x-ms-request-id: f4190a32-701e-0065-6df0-c1eab5000000 x-ms-version: 2009-09-19 x-ms-lease-status: unlocked x-ms-blob-type: BlockBlob Access-Control-Allow-Origin: * Date: Fri, 15 Oct 2021 18:15:11 GMT     8  0�40�� ��0�2I�   ��0	*�H�� 0O10	UUS10U
Microsoft Corporation1 0UMicrosoft RSA TLS CA 020210906090832Z220906090832Z0"1 0U*.blob.core.windows.net0�"0	*�H�� � 0�
� ��dc"�B�T"��>q(�lT��q��UDWկ<��٭p	]րD�?�1l�s|�����#I��<A`��e�ղ�|�vm����Xz�(���e]�t�8��Q��wSnr�c��c}n�Gr�d�U�|~x?c�9)zg��?DN&첀4�2�\£>A�C}��m�|e� ��aC����|u>�.e+����cK��R���d&�F�\Al.Hg��K\F�g�B�)Sx���ٛ�'hu�P�n�  ��
40�
00�~
+�y�n�jh w F�U�u�� 0���i��},At��I�����p�mG  {�f��   H0F! ����?PGQ��S��J��<�et
߫�@�! �qP�R=��+Cy�
����َ�gw|b� u U����6J��W<S���8xp%/���  {�f�c   F0D x��5���鮡)	�b���3+
ls��� ;B��s�n�G�3'���=$0���4��:u��� v A�ʱ�"FJơ:	B�^N1���K�h�b��  {�f��   G0E! �Du�[�g+�R}R]�f�
�諊�6.'�a� ��2�8Vˮ��n�V�_��ɫ�T����*�R0'	+�7
00
+0
+0>	+�710/'+�7�چu����Ʌ���a���`�]���A�Pd'0��+{0y0S+0�Ghttp://www.microsoft.com/pki/mscorp/Microsoft%20RSA%20TLS%20CA%2002.crt0"+0�http://ocsp.msocsp.com0U@ڄ	tuy���5�V$���z0U��0�<U�30�/�*.blob.core.windows.net�'*.dub09prdstr08a.store.core.windows.net�*.blob.storage.azure.net�*.z1.blob.storage.azure.net�*.z2.blob.storage.azure.net�*.z3.blob.storage.azure.net�*.z4.blob.storage.azure.net�*.z5.blob.storage.azure.net�*.z6.blob.storage.azure.net�*.z7.blob.storage.azure.net�*.z8.blob.storage.azure.net�*.z9.blob.storage.azure.net�*.z10.blob.storage.azure.net�*.z11.blob.storage.azure.net�*.z12.blob.storage.azure.net�*.z13.blob.storage.azure.net�*.z14.blob.storage.azure.net�*.z15.blob.storage.azure.net�*.z16.blob.storage.azure.net�*.z17.blob.storage.azure.net�*.z18.blob.storage.azure.net�*.z19.blob.storage.azure.net�*.z20.blob.storage.azure.net�*.z21.blob.storage.azure.net�*.z22.blob.storage.azure.net�*.z23.blob.storage.azure.net�*.z24.blob.storage.azure.net�*.z25.blob.storage.azure.net�*.z26.blob.storage.azure.net�*.z27.blob.storage.azure.net�*.z28.blob.storage.azure.net�*.z29.blob.storage.azure.net�*.z30.blob.storage.azure.net�*.z31.blob.storage.azure.net�*.z32.blob.storage.azure.net�*.z33.blob.storage.azure.net�*.z34.blob.storage.azure.net�*.z35.blob.storage.azure.net�*.z36.blob.storage.azure.net�*.z37.blob.storage.azure.net�*.z38.blob.storage.azure.net�*.z39.blob.storage.azure.net�*.z40.blob.storage.azure.net�*.z41.blob.storage.azure.net�*.z42.blob.storage.azure.net�*.z43.blob.storage.azure.net�*.z44.blob.storage.azure.net�*.z45.blob.storage.azure.net�*.z46.blob.storage.azure.net�*.z47.blob.storage.azure.net�*.z48.blob.storage.azure.net�*.z49.blob.storage.azure.net�*.z50.blob.storage.azure.net0��U��0��0���������Mhttp://mscrl.microsoft.com/pki/mscorp/crl/Microsoft%20RSA%20TLS%20CA%2002.crl�Khttp://crl.microsoft.com/pki/mscorp/crl/Microsoft%20RSA%20TLS%20CA%2002.crl0WU P0N0B	+�7*0503+'http://www.microsoft.com/pki/mscorp/cps0g�0U#0��/��8�-�%�����l��0U%0++0	*�H�� � p|��a,�Cv*�/2.�o�X��n��ޤ��WyCTg@��0|�Z^)�^���F��a��*R	̕=X*�jIUS�Mh���'��A1d��c���y
e��=��A��(��E�@�80�|�����h�p��R\A�v��;U�� ����.�Z�BX^�?μFH��$"�/ڗ"P^q�
c�1�G���NAb�py>�t���%U��w�2֔1]Cq���Nͪ��R��d����R�B�QS�ث���3�d2YV��NKE�ŇRg��z�� Ww����tby�os4bI��%艙
=.��Nz�:��Z�]Bs�� +h��K�C��M�|G�<�N呧�`�]"�\Z
�� �	 ����yܹ�O�%jO��8�8Y�������Ψ��=�IM���xy��h[�)Fo�o)�����?���>���Ф�,�ֱ�v�iA6�}��Wg����D��z�՛�!��x�uZ�=�������^  0�Z0�B��G"�=��X���J:0	*�H�� 0Z10	UIE10U
	Baltimore10U
CyberTrust1"0 UBaltimore CyberTrust Root0200721230000Z241008070000Z0O10	UUS10U
Microsoft Corporation1 0UMicrosoft RSA TLS CA 020�"0	*�H�� � 0�
� ��Y�*$|!1����j����J/�]�.)8�̕])��]7� ״?X:z��<�+
r�@�닎�������<%�l3�VO���H�����8o�� ��2*l)�]n�#��X�����mG��@�}sB��ZFK�)O>}�9|^#�gT�((�����qi�8IV�r#���)87fz ��1���A'�S��>��eڔ�]s4�hj����N]u��D[�}�*j�蟿�w
�k���n�'�@k)U&G�#�_��,Q�:�{�Z���K�B�e+�"6V(���(�$�^X�B>�'$!*\�*0�|���e
@*��c��/�Yz���C�ԁ�][F4/Ԣ��"G�y���6��1o�m ��K�h��Y�|���J�pLy��"�%Sد��O[�Y���)ZŁ�4���㡟�ཱུ�ەI�b�2e��<�~b�j��{ۇ�v����/0nf�QdN��í�7#�ͩj~-�n|�I����uB�Sꌟ ��%0�!0U�/��8�-�%�����l��0U#0��Y0�GX̬�T6�{:�M�0U��0U%0++0U�0� 04+(0&0$+0�http://ocsp.digicert.com0:U3010/�-�+�)http://crl3.digicert.com/Omniroot2025.crl0*U #0!0g�0g�0	+�7*0	*�H�� � ���z��5�t� �7��K�^.1�5�;q��YӉ�KA�Cr����R3Ry���ڔ����v���� �����K#��)A��5�vCn����>>�d2:|�v�=���/%ByC>������+CŨ���*�z��u]�q��"5%�n��R�a�D|�c-�����Ck`��
��q��Q+Q��{޸�|�Jp4����Fj?���>X�RdmAn��O��U�5ȡ�t[�Dе�O��*wげ��|	  {  0�w0�_�  �0	*�H�� 0Z10	UIE10U
	Baltimore10U
CyberTrust1"0 UBaltimore CyberTrust Root0000512184600Z250512235900Z0Z10	UIE10U
	Baltimore10U
CyberTrust1"0 UBaltimore CyberTrust Root0�"0	*�H�� � 0�
� ��"��=W�&r��y�)��蕀���[�+)�dߡ]��	m�(.�b�b�����8�!��A+R{�w��Ǻ���j	�s�@����b�-��PҨP�(���%�����g�?���R/��pp����˚���3zw����hDBH��¤�^`������Y�Y�c��c��}]�z�����^�>_��i��96ru�wRM�ɐ,�=��#S?$�!\�)��:��n�:k�tc3�h1�x�v����]*��M��'9 �E0C0U�Y0�GX̬�T6�{:�M�0U�0�0U�0	*�H�� � �]��oQhB�ݻO'%���d�-�0���))�y?v�#�
�X��ap�aj��
�ż0|��%��@O�̣~8�7O��h1�Lҳt�u^Hp�\��y����e����R79թ1z��*����E��<^����Ȟ|.Ȥ�NKm�pmkc�d�����.���P�s������2�����~5���>0�z�3=�e����b�GD,]��2�G�8.����2j��<����$B�c9�     0�P 
   20.60.40.4  �          �־9ֆ�>t��`X����[�9V��ҽ�A�Eo��   �$81      
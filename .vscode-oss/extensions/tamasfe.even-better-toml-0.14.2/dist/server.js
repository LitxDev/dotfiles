'use strict';

var fs = require('fs');
var path = require('path');
var process$1 = require('process');
var index = require('./index-2afa2cd2.js');
require('url');
require('stream');
require('http');
require('https');
require('zlib');

function _interopNamespace(e) {
  if (e && e.__esModule) return e;
  var n = Object.create(null);
  if (e) {
    Object.keys(e).forEach(function (k) {
      if (k !== 'default') {
        var d = Object.getOwnPropertyDescriptor(e, k);
        Object.defineProperty(n, k, d.get ? d : {
          enumerable: true,
          get: function () {
            return e[k];
          }
        });
      }
    });
  }
  n['default'] = e;
  return Object.freeze(n);
}

var fs__namespace = /*#__PURE__*/_interopNamespace(fs);
var path__namespace = /*#__PURE__*/_interopNamespace(path);

// Wrapper over the WASM module.
// For reqwest
global.Headers = index.Headers;
global.Request = index.Request;
global.Response = index.Response;
global.Window = Object;
global.fetch = index.fetch;
let taplo;
process.on("message", async (d) => {
    if (d.method === "exit") {
        process$1.exit(0);
    }
    if (typeof taplo === "undefined") {
        taplo = await index.lsp.TaploLsp.initialize({
            isWindows: () => {
                return process.platform == "win32";
            },
            sendMessage: (msg) => {
                if (process.send) {
                    process.send(msg);
                }
            },
            readFile: (path) => {
                return fs__namespace.promises.readFile(path);
            },
            writeFile: (path, data) => {
                return fs__namespace.promises.writeFile(path, data);
            },
            isAbsolutePath: (p) => {
                return (path__namespace.resolve(p) ===
                    path__namespace.normalize(p).replace(RegExp(path__namespace.sep + "$"), ""));
            },
            fileExists: (p) => {
                return fs__namespace.existsSync(p);
            },
            mkdir: (p) => {
                fs__namespace.mkdirSync(p, { recursive: true });
            },
            needsUpdate: (path, newDate) => fs__namespace.statSync(path).mtimeMs < newDate,
        });
    }
    taplo.message(d);
});
// These are panics from Rust.
process.on("unhandledRejection", up => {
    throw up;
});

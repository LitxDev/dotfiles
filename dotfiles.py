import os
import argparse

parser = argparse.ArgumentParser()

parser.add_argument(
    "--push",
    action="store_true",
    help="copies the dotfiles from config and home to the dotfiles repo",
)
parser.add_argument(
    "--pull",
    action="store_true",
    help="copies the dotfiles from the repo to config and home",
)

args = parser.parse_args()

# the dotfiles in ~/.config
config_dotfiles = [
    "cava",
    "fish/config.fish",
    "kitty",
    "neofetch",
    "sway",
    "waybar",
    "starship.toml",
    "wofi",
    "VSCodium",
]
# the dotfiles in ~/
home_dotfiles = [".gitconfig", ".npmrc", ".vscode-oss"]

# copies the dotfiles from config and home to the dotfiles repo
def push():
    print("Pushing files...")
    for dotfile in config_dotfiles:
        os.system(f"cp -r ~/.config/{dotfile} ~/dotfiles/")

    for dotfile in home_dotfiles:
        os.system(f"cp -r ~/{dotfile} ~/dotfiles/")
    print("Done.")


# copies the dotfiles from the repo to config and home
def pull():
    print("Pulling files...")
    for dotfile in config_dotfiles:
        os.system(f"cp -r ~/dotfiles/{dotfile} ~/.config/")

    for dotfile in home_dotfiles:
        os.system(f"cp -r ~/dotfiles/{dotfile} ~/")
    print("Done.")

if args.push:
    push()
elif args.pull:
    pull()
